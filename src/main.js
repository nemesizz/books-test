import App from './App.vue'
import Vue from 'vue'

import 'bootstrap'
import 'bootstrap/dist/css/bootstrap.min.css'
import '@fortawesome/fontawesome-free/css/all.css'
import '@fortawesome/fontawesome-free/js/all.js'

window.jQuery = window.$ = require('../node_modules/jquery/dist/jquery');

Vue.config.productionTip = false

new Vue({
  render: h => h(App),
}).$mount('#app')
