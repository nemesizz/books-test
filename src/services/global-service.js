/* eslint-disable */
import firebase from 'firebase/app'
import 'firebase/firestore'

// Your web app's Firebase configuration
const firebaseConfig = {
    apiKey: "AIzaSyCtv7zVUgbeT2LpCoOmgjYjqYfJil45NME",
    authDomain: "test-books-c1191.firebaseapp.com",
    projectId: "test-books-c1191",
    storageBucket: "test-books-c1191.appspot.com",
    messagingSenderId: "653393465605",
    appId: "1:653393465605:web:504ce7f45c4e89cc3e676c"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig)

const db = firebase.firestore()
const authorsRef = db.collection('authors')
const booksRef = db.collection('books')

class globalServiceClass {
    deleteAuthor(id) {
        authorsRef.doc(id).delete()
    }

    deleteBook(id) {
        booksRef.doc(id).delete()
    }

    getAuthors() {
        let authors = [];

        authorsRef.get()
            .then((resp) => {
                resp.docs.map((item) => {
                    authors.push({
                        id: item.id,
                        data: item.data()
                    })
                })
            })

        return authors;
    }

    getBooks() {
        let books = [];

        booksRef.get()
            .then((resp) => {
                resp.docs.map((item) => {
                    books.push({
                        id: item.id,
                        data: item.data()
                    })
                })
            })

        return books;
    }

    postAuthor(author) {
        authorsRef.add(author)
    }

    postBook(book) {
        booksRef.add(book)
    }

    putAuthor(id, author) {
        authorsRef.doc(id).set(author)
    }

    putBook(id, book) {
        booksRef.doc(id).set(book)
    }
}

const globalService = new globalServiceClass();
export default globalService;
